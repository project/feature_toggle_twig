Feature Toggle Twig
===================

Integrates the [Feature Toggle module](https://www.drupal.org/project/feature_toggle)
with Twig templates.

## Usage

This module adds a `featureIsEnabled` function to Twig that allows for checking
if a given feature flag is enabled.

For example:

```twig
{% if featureIsEnabled('foo') %}
  {# ... #}
{% endif %}
```
