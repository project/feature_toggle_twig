<?php

namespace Drupal\feature_toggle_twig\TwigExtension;

use Drupal\feature_toggle\FeatureStatusInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class FeatureToggleTwigExtension extends AbstractExtension {

  private FeatureStatusInterface $featureStatus;

  public function __construct(FeatureStatusInterface $featureStatus) {
    $this->featureStatus = $featureStatus;
  }

  public function getFunctions() {
    return [
      new TwigFunction('featureIsEnabled', [$this, 'featureIsEnabled']),
    ];
  }

  public function getName() {
    return 'feature_toggle.twig_extension';
  }

  public function featureIsEnabled(string $name): bool {
    return $this->featureStatus->getStatus($name);
  }

}
